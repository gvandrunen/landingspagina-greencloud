var iframe = document.getElementById("modal-video");
var player = new Vimeo.Player(iframe);

document.getElementById('hero-play').addEventListener('click', function() {
    document.querySelector('.modal').classList.add('modal--active');
    player.play();
});

document.getElementById('close-modal').addEventListener('click', function() {
    closeModal();
});

document.getElementById('modal-bg').addEventListener('click', function() {
    closeModal();
});

function closeModal() {
    document.querySelector('.modal').classList.remove('modal--active');
    player.unload();
}
